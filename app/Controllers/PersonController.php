<?php

namespace App\Controllers;

use Models\Person;
use Models\PersonQuery;


class PersonController {

    /**
     * Returns an error message
     *
     * @param string $errorMessage
     * @param int $statusCode
     * @return string
     */
    public function showError($errorMessage, $statusCode) {
        http_response_code($statusCode);
        return json_encode([ 'error' => $errorMessage ]);
    }

    /**
     * Create a Person instance
     *
     * Data must be in the form
     * {
     *   "FirstName": "",
     *   "LastName": "",
     *   "Address": "",
     *   "Age": 0,
     * }
     *
     * @return string
     * @throws \Propel\Runtime\Exception\PropelException
     */
	public function createPerson() {
		$data = json_decode(file_get_contents('php://input'));

        header('Content-Type: application/json');

        // Check if the required parameters are present
        if (!isset($data->FirstName) || !isset($data->LastName) || !isset($data->Age)) {
            return $this->showError('Missing Parameters', 422);
        }

        // Check if age is a number
        if (!is_numeric($data->Age)) {
            return $this->showError('Age must be a number', 422);
        }

        // create a person instance
        $person = new Person();

        $person->setFirstName($data->FirstName);
        $person->setLastName($data->LastName);
        $person->setAge($data->Age);

        // only set address when it is defined
        if (isset($data->Address)) {
            $person->setAddress($data->Address);
        }
        $person->save();

        http_response_code(200);
        return $person->toJSON();
	}

    /**
     * Returns a collection of Persons under property `people`
     *
     * @return string
     */
	public function getList() {
		$data = PersonQuery::create()->find();

        header('Content-Type: application/json');
        http_response_code(200);

        return $data->toJSON();
	}

    /**
     * Get instance of a person given the ID
     *
     * @param int $id
     * @return string
     */
    public function getPerson($id) {
        $person = PersonQuery::create()->findPk($id);

        header('Content-Type: application/json');

        if ($person == null) {
            return $this->showError('Cannot find person', 404);
        }

        http_response_code(200);
        return $person->toJSON();
    }

    /**
     * Updates the Person's details.
     * All properties except address are required when updating, as recommended when using PUT
     *
     * @param int $id
     * @return string
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function updatePerson($id) {
        $data = json_decode(file_get_contents('php://input'));
        $person = PersonQuery::create()->findPk($id);

        header('Content-Type: application/json');

        if ($person == null) {
            return $this->showError('Cannot find person', 404);
        }

        // Check if the required parameters are present
        if (!isset($data->FirstName) || !isset($data->LastName) || !isset($data->Age)) {
            return $this->showError('Missing Parameters', 422);
        }

        // Check if age is a number
        if (!is_numeric($data->Age)) {
            return $this->showError('Age must be a number', 422);
        }

        $person->setFirstName($data->FirstName);
        $person->setLastName($data->LastName);
        $person->setAge($data->Age);

        // only set address when it is defined
        if (isset($data->Address)) {
            $person->setAddress($data->Address);
        }
        $person->save();

        http_response_code(200);
        return $person->toJSON();
    }

    /**
     * Patch a person's details with the given ID. Unlike `updatePerson`, this does not require properties to be
     * present.
     *
     * @param $id
     * @return string
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function patchPerson($id) {
        $data = json_decode(file_get_contents('php://input'));
        $person = PersonQuery::create()->findPk($id);

        header('Content-Type: application/json');

        if ($person == null) {
            return $this->showError('Cannot find person', 404);
        }

        if (isset($data->FirstName)) {
            $person->setFirstName($data->FirstName);
        }

        if (isset($data->LastName)) {
            $person->setLastName($data->LastName);
        }

        if (isset($data->Age)) {

            // Check if age is a number
            if (!is_numeric($data->Age)) {
                return $this->showError('Age must be a number', 422);
            }

            $person->setAge($data->Age);
        }

        if (isset($data->Address)) {
            $person->setAddress($data->Address);
        }

        $person->save();

        http_response_code(200);
        return $person->toJSON();
    }

    /**
     * Deletes a person with the given ID
     *
     * @param int $id
     * @return string
     * @throws \Propel\Runtime\Exception\PropelException
     */
    public function deletePerson($id) {
        $person = PersonQuery::create()->findPk($id);

        header('Content-Type: application/json');

        if ($person == null) {
            return $this->showError('Cannot find person', 404);
        }

        $person->delete();

        http_response_code(200);
        return json_encode([ 'message' => 'Successfully deleted' ]);
    }
}
