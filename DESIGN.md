### Technical Design Decisions

Router Library: `phrouter`

* Api is considerably easier than other php-routers out there
* Api also resembles more popular routers from other languages/framework

ORM Library: propel2
* Current version is actually 3, but it lacks documentation as of writing
* API is similar to other ORM libraries and web frameworks with ORM capabilities

