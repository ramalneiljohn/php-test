# Diliman Labs PHP Test

Your goal is to create a simple REST API service _without_ the help of a full stack web framework<sup>*</sup>

\* e.g. Laravel, CodeIgniter, Symfony (use of individual Symfony components is allowed)

## Requirements

* All your code should be saved in a fork of this repo
* The root route (`/`) should return this response

```
{
    "ack": <unix_time>
}
```
* 4 other routes should be mapped to CRUD operations of the `Person` entity
* Provide brief documentation explaining your technical design choices
* Provide instructions on how to run your app
  * Setup instructions should be as simple as possible
  * Assume a fresh install Linux environment
* Create a pull request to this repo with your name as the title once done

## Criteria

* tech stack
* object oriented design implementation
* unit tests

## Time allowance

**72 hours** after the repo invitation has been accepted. Approval of extra time request is subject to assumed skill level based on CV.

> (c) 2017+ Diliman Labs
