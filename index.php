<?php
require __DIR__.'/vendor/autoload.php';
require_once __DIR__ . '/generated-conf/config.php';

use Phroute\Phroute\RouteCollector;
use Phroute\Phroute\Dispatcher;

$collector = new RouteCollector();

$collector->get('/', function() {
	header('Content-Type: application/json');
	return json_encode([ 'ack' => time() ]);
});

// CRUD operations for `Person` entity
$collector->post(   '/persons',      ['App\Controllers\PersonController', 'createPerson']);
$collector->get(    '/persons/{id}', ['App\Controllers\PersonController', 'getPerson']);
$collector->patch(  '/persons/{id}', ['App\Controllers\PersonController', 'patchPerson']);
$collector->delete( '/persons/{id}', ['App\Controllers\PersonController', 'deletePerson']);

// $collector->get(    '/persons',      ['App\Controllers\PersonController', 'getList']);
// $collector->put(    '/persons/{id}', ['App\Controllers\PersonController', 'updatePerson']);

$dispatcher = (new Dispatcher($collector->getData()));
echo $dispatcher->dispatch($_SERVER['REQUEST_METHOD'], $_SERVER['REQUEST_URI']);
