### Instructions to run

Make sure `sqlite3`, `php`, and `composer` are installed on your system

1. Change directory to root project
2. Run `composer install` to download the dependencies.
3. Run `composer dump-autoload` to include all necessary classes in the project
4. Copy and rename `propel.yml.dist` to `propel.yml` and replace the necessary fields
5. Create an sqlite file (i.e. `touch person.sqlite`) matching that with the `dsn` field in the `propel.yml` file
6. Run `vendor/bin/propel sql:build` to generate the sql file
6. Run `vendor/bin/propel sql:insert` to initialize the database
7. Run `vendor/bin/propel config:convert` to generate the config file
8. Run `php -S localhost:<PORT>` if project is not under Apache or nginx replacing `<PORT>` with the desired port number (e.g. 8000)
